﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;

namespace PO.Scanner.App.Models
{
    class BGValidation
    {
        private int bGValidationId;
        private string bGNumber;
        private string validationStatus;
        private DateTime validationTime;
        private string validatedBy;
        private int status;
        private string scanBody;

        public virtual int BGValidationId { get { return bGValidationId; } set { bGValidationId = value; } }
        public virtual string BGNumber { get { return bGNumber; } set { bGNumber = value; } }
        public virtual string ValidationStatus { get { return validationStatus; } set { validationStatus = value; } }
        public virtual DateTime ValidationTime { get { return validationTime; } set { validationTime = value; } }
        public virtual string ValidatedBy { get { return validatedBy; } set { validatedBy = value; } }
        public virtual int Status { get { return status; } set { status = value; } }
        public virtual string ScanBody { get { return scanBody; } set { scanBody = value; } }

        public BGValidation()
        {

        }

        public bool Save()
        {
            try
            {
                using (ISession session = SessionFactory.OpenSession)
                {
                    session.Save(this);
                }
                
                return true;
            }
            catch (Exception exc)
            {

            }

            return false;
        }

    }
}
