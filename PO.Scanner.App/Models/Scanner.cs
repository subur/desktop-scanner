﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO.Scanner.App.Models
{
    class Scanner
    {
        private int scannerId;
        private string serialNumber;
        private int status;
        private DateTime? createdTime;
        private DateTime? updatedTime;
        private string createdBy;
        private string updatedBy;


        public virtual int ScannerId { get { return scannerId; } set { scannerId = value; } }
        public virtual string SerialNumber { get { return serialNumber; } set { serialNumber = value; } }
        public virtual int Status { get { return status; } set { status = value; } }
        public virtual DateTime? CreatedTime { get { return createdTime; } set { createdTime = value; } }
        public virtual DateTime? UpdatedTime { get { return updatedTime; } set { updatedTime = value; } }
        public virtual string CreatedBy { get { return createdBy; } set { createdBy = value; } }
        public virtual string UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }


        public Scanner()
        {

        }
    }
}
