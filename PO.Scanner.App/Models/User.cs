﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;

namespace PO.Scanner.App.Models
{
    class User
    {
        private string username;
        private string password;
        private string name;
        private int status;
        private DateTime? createdTime;
        private DateTime? updatedTime;
        private string createdBy;
        private string updatedBy;
        private Scanner scanner;

        public virtual String Username { get { return username; } set { username = value; } }
        public virtual String Password { get { return password; } set { password = value; } }
        public virtual String Name { get { return name; } set { name = value; } }
        public virtual int  Status { get { return status; } set { status = value; } }
        public virtual DateTime? CreatedTime { get { return createdTime; } set { createdTime = value; } }
        public virtual DateTime? UpdatedTime { get { return updatedTime; } set { updatedTime = value; } }
        public virtual string CreatedBy { get { return createdBy; } set { createdBy = value; } }
        public virtual string UpdatedBy { get { return updatedBy; } set { updatedBy = value; } }

        public virtual Scanner Scanner { get { return scanner; } set { scanner = value; } }

        public User()
        {

        }

        public List<User> UserList()
        {
            List<User> userList = new List<User>();
            using (ISession session = SessionFactory.OpenSession)
            {
                var sc = session.CreateCriteria(typeof(User));
                userList = (List<User>)sc.List<User>();
                session.Close();
            }
            return userList;
        }

        public User Login(string username, string password)
        {
            User listUser;
            using (ISession session = SessionFactory.OpenSession)
            {
                var sc = session.CreateCriteria<User>()
                    .Add(Expression.Eq("Username", username))
                    .Add(Expression.Eq("Password", password))
                    .UniqueResult<User>();

                listUser = sc;
            }
            
            return listUser;
        }

        public static User FindUser(string username)
        {
            User listUser;
            using (ISession session = SessionFactory.OpenSession)
            {
                var sc = session.CreateCriteria<User>()
                    .Add(Expression.Eq("Username", username))
                    .UniqueResult<User>();

                listUser = sc;
            }

            return listUser;
        }

    }

}
