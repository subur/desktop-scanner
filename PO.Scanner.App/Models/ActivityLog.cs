﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;

namespace PO.Scanner.App.Models
{
    class ActivityLog
    {
        private int logId;
        private DateTime? logTime;
        private string logName;
        private string remarks;

        public virtual int LogId { get { return logId; } set { logId = value; } }
        public virtual DateTime? LogTime { get { return logTime; } set { logTime = value; } }
        public virtual string LogName { get { return logName; } set { logName = value; } }
        public virtual string Remarks { get { return remarks; } set { remarks = value; } }

        public ActivityLog()
        {

        }

        public bool Save()
        {
            try
            {
                using (ISession session = SessionFactory.OpenSession)
                {
                    session.Save(this);
                }

                return true;
            }
            catch (Exception exc)
            {
                
            }

            return false;
        }

    }
}
