﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PO.Scanner.App.Connection
{
    class Connection
    {
        private MySqlConnection connection;

        public Connection()
        {
            Initialize();

        }
        private void Initialize()
        {
            string connectionStrings = ConfigurationManager.ConnectionStrings["connection_string_name"].ConnectionString;
            string[] explode = connectionStrings.Split(';');
            string[] vServer = explode[0].Split('=');
            string[] vDatabase = explode[1].Split('=');
            string[] vUserID = explode[2].Split('=');
            string[] vPassword = explode[3].Split('=');
            var connectionString = string.Format("server=" + vServer[1] + ";"
                                                 + "user id=" + vUserID[1] + ";"
                                                 + "password=" + vPassword[1] + ";"
                                                 + "persistsecurityinfo=True;"
                                                 + "port=3306;"
                                                 + "database=" + vDatabase[1] + ";"
                                                 + "SslMode=none");
            connection = new MySqlConnection(connectionString);
            connection.Close();
        }


        private bool OpenConnection()
        {
            var status = false;
            try
            {
                connection.Open();
                status = true;

            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show(@"Connection Error !", @"ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case 1045:
                        MessageBox.Show(@"Invalid username / password Database", @"ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
            }
            return status;
        }

        private void CloseConnection()
        {
            try
            {
                connection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message, @"ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public DataSet DataGridSelect(string table, string query)
        {

            var data = new DataSet();
            try
            {
                if (OpenConnection())
                {
                    var dataAdapter = new MySqlDataAdapter(query, connection);
                    dataAdapter.Fill(data, table);
                    CloseConnection();
                }
            }
            catch (MySqlException ex)
            {

                MessageBox.Show(ex.Message, @"ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                data = null;
            }
            return data;
        }
    }
}
