﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO.Scanner.App.Connection
{
    class Stringformat
    {
        public string GetTimestamp(DateTime dateTime)
        {
            return dateTime.ToString("ddMMyyyyHHmmss");
        }

        public string GetTimestamp2(DateTime dateTime)
        {
            return dateTime.ToString("dd/MMM/yyyy HH:mm:ss");
        }
    }
}
