﻿using com.google.zxing;
using com.google.zxing.qrcode;
using iTextSharp.text;
using iTextSharp.text.pdf;
using PO.Scanner.App.Connection;
using PO.Scanner.App.Models;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace PO.Scanner.App.Forms
{
    public partial class FormScan : Form
    {
        private bool formLogout = false;
        delegate void SetTextCallback(string text);
        private static FormMessage msg = new FormMessage();
        private bool retry = true;
        private bool serialIsConnected = false;
        string[] dataPublic = new string[0];
        string[] dataPrivate = new string[0];
        Stringformat instance = new Stringformat();

        public FormScan()
        {
            InitializeComponent();

            //COLOR GRID

            gridPublicInfo.DefaultCellStyle.ForeColor = Color.Blue;
            gridPrivateInfo.DefaultCellStyle.ForeColor = Color.Red;
            lblUsername.Text = "Welcome " + UserInfo.name;
            lblSerialNumber.Text = "SN : " + UserInfo.ScannerSerialNumber;

            QRCodeWriter writer = new QRCodeWriter();
            btnValid.Image = writer.encode("action=validate", BarcodeFormat.QR_CODE, 150, 150).ToBitmap();
            btnNew.Image = writer.encode("action=new", BarcodeFormat.QR_CODE, 150, 150).ToBitmap();            
            btnValid.BackColor = Color.White;
            btnNew.BackColor = Color.White;

            msg.setText("Apakah data Peruri Code sudah sesuai dengan data BG ?");

            serialPort.PortName = System.Configuration.ConfigurationManager.AppSettings.Get("COM").ToString();

            while (retry && !serialIsConnected) {
                try
                {
                    serialPort.Open();
                    serialIsConnected = true;

                    Thread.Sleep(500);
                    //serialPort.WriteLine("BH3\r");
                    serialPort.WriteLine("IDF\r");

                    
                }
                catch (Exception exc)
                {
                    string exceptionMessage = exc.Message;

                    ActivityLog log = new ActivityLog();
                    log.LogTime = DateTime.Now;
                    log.LogName = "ScannerConnection";
                    log.Remarks = string.Format("{0}|{1}|{2}|{3}", UserInfo.username, "Serial Port Open", "Failed",exc.Message);
                    log.Save();

                    DialogResult result = MessageBox.Show("Tidak dapat terkoneksi dengan scanner, Coba Kembali?", "Scanner Disconnected", MessageBoxButtons.RetryCancel);
                    if (result == DialogResult.Retry)
                    {
                        retry = true;
                    }
                    else
                    {
                        retry = false;
                    }
                }
            }
            

            if (!serialIsConnected)
            {
                DialogResult result = MessageBox.Show("Tidak dapat terkoneksi dengan scanner, Mengakhiri Aplikasi", "Application Closing", MessageBoxButtons.OK);
                LogoutFunction();
                formLogout = true;
                this.Close();
                MainForm main = new MainForm();
                main.Show();
            }

            
        }

        private void FormScan_FormClosing(object sender, FormClosingEventArgs e)
        {
            LogoutFunction();
        }

        private void LogoutFunction()
        {
            ActivityLog log = new ActivityLog();
            log.LogName = "Logout";
            log.LogTime = DateTime.Now;
            log.Remarks = string.Format("{0}|{1}|{2}", UserInfo.username, "Logout", "Success");
            log.Save();

            UserInfo.username = null;
            UserInfo.status = null;
            UserInfo.name = null;
        }

        private void FormScan_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(formLogout == true)
            {
                MainForm mf = new MainForm();
                mf.Show();                
            }
            else
            {

                ActivityLog log = new ActivityLog();
                log.LogName = "App Close";
                log.LogTime = DateTime.Now;
                log.Remarks = string.Format("{0}|{1}|{2}", UserInfo.username, "App Close", "Success");
                log.Save();
                Application.Exit();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LogoutFunction();
            formLogout = true;
            this.Close();
        }

        private void btnValid_Click(object sender, EventArgs e)
        {
            if(gridPrivateInfo.Rows.Count > 0 || gridPublicInfo.Rows.Count > 0) { 
                DialogResult res = msg.ShowDialog();
                //convert grid to string
                int i = 0;
                StringBuilder sbPublic = new StringBuilder();
                StringBuilder sbPrivate = new StringBuilder();
                string bgNumber = string.Empty;
                foreach (DataGridViewRow r in gridPublicInfo.Rows)
                {
                    sbPublic.Append(r.Cells[1].Value.ToString());
                }
                foreach (DataGridViewRow r in gridPrivateInfo.Rows)
                {
                    if(i == 1)
                    {
                        bgNumber = r.Cells[1].Value.ToString();
                    }
                    sbPrivate.Append(r.Cells[1].Value.ToString());
                    i++;
                }
                if (res == DialogResult.Yes)
                {
                    BGValidation bg = new BGValidation();
                    bg.Status = 0;
                    bg.ValidationStatus = "VALID";
                    bg.BGNumber = bgNumber;
                    bg.ValidatedBy = UserInfo.username;
                    bg.ValidationTime = DateTime.Now;
                    bg.ScanBody = string.Format("{0}{1}", sbPublic.ToString(), sbPrivate.ToString());
                    bg.Save();

                    ActivityLog log = new ActivityLog();
                    log.LogName = "Validation Success";
                    log.LogTime = DateTime.Now;
                    log.Remarks = string.Format("{0}|{1}|{2}|{3}", UserInfo.username, "Data Validation", "Success",gridPrivateInfo.Rows);
                    log.Save();

                    lblScanInfo.BackColor = Color.DarkGreen;
                    lblScanInfo.ForeColor = Color.White;
                    //lblScanInfo.Text = "BG Data Validation Success";
                    setScanStatus("BG Data Validation Success");
                }
                else
                {
                    BGValidation bg = new BGValidation();
                    bg.Status = 0;
                    bg.ValidationStatus = "INVALID";
                    bg.ValidatedBy = UserInfo.username;
                    bg.ValidationTime = DateTime.Now;
                    bg.ScanBody = string.Format("{0}{1}", sbPublic.ToString(), sbPrivate.ToString());
                    bg.Save();

                    ActivityLog log = new ActivityLog();
                    log.LogName = "Validation Failed";
                    log.LogTime = DateTime.Now;
                    log.Remarks = string.Format("{0}|{1}|{2}|{3}", UserInfo.username, "Data Validation", "Failed", bg.ScanBody);
                    log.Save();

                    lblScanInfo.BackColor = Color.Red;
                    lblScanInfo.ForeColor = Color.White;
                    //lblScanInfo.Text = "BG Data Validation Failed";
                    setScanStatus("BG Data Validation Failed");
                }
            }
            else
            {
                MessageBox.Show("Tidak ada data BG, Silahkan Scan data terlebih dahulu.", "No Data Found", MessageBoxButtons.OK);
            }
        }
        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string str = string.Empty;
            byte _terminator = 0x0d;
            byte[] buffer = new byte[serialPort.ReadBufferSize];
            
            int bytesRead = serialPort.Read(buffer, 0, buffer.Length);
            
            str += Encoding.ASCII.GetString(buffer, 0, bytesRead);

            if (str.IndexOf((char)_terminator) > -1)
            {
             
                string workingString = str.Substring(0, str.IndexOf((char)_terminator));

                Console.WriteLine(workingString);

                try
                {
                    ActivityLog log = new ActivityLog();
                    log.LogName = "New BG Validation";
                    log.LogTime = DateTime.Now;
                    log.Remarks = string.Format("{0}|{1}|{2}|{3}", workingString, "New BG Validation", "Success", "");
                    log.Save();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }

                if (str.Contains("action=validate"))
                {
                    this.btnValid_Click(this, e);

                    /*
                    if (gridPrivateInfo.Rows.Count > 0 || gridPublicInfo.Rows.Count > 0)
                    {
                        FormMessage msg = new FormMessage("Apakah data SQRC sudah sesuai dengan data BG ?");
                        DialogResult res = msg.ShowDialog();
                        
                        if (res == DialogResult.Yes)
                        {
                            try
                            {
                                BGValidation bg = new BGValidation();
                                bg.Status = 0;
                                bg.ValidationStatus = "VALID";
                                bg.ValidatedBy = UserInfo.username;
                                bg.ValidationTime = DateTime.Now;
                                bg.ScanBody = str;
                                bg.Save();

                                ActivityLog log = new ActivityLog();
                                log.LogName = "Validation Success";
                                log.LogTime = DateTime.Now;
                                log.Remarks = string.Format("{0}|{1}|{2}|{3}", UserInfo.username, "Data Validation", "Success", bg.ScanBody);
                                log.Save();

                                lblScanInfo.BackColor = Color.DarkGreen;
                                lblScanInfo.ForeColor = Color.White;

                                setScanStatus("BG Data Validation Success");
                            } catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            try {
                                BGValidation bg = new BGValidation();
                                bg.Status = 0;
                                bg.ValidationStatus = "INVALID";
                                bg.ValidatedBy = UserInfo.username;
                                bg.ValidationTime = DateTime.Now;
                                bg.ScanBody = str;
                                bg.Save();

                                ActivityLog log = new ActivityLog();
                                log.LogName = "Validation Failed";
                                log.LogTime = DateTime.Now;
                                log.Remarks = string.Format("{0}|{1}|{2}|{3}", UserInfo.username, "Data Validation", "Failed", bg.ScanBody);
                                log.Save();

                                lblScanInfo.BackColor = Color.Red;
                                lblScanInfo.ForeColor = Color.White;

                                setScanStatus("BG Data Validation Failed");
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Tidak ada data BG, Silahkan Scan data terlebih dahulu.", "No Data Found", MessageBoxButtons.OK);
                    }
                    */
                    
                }
                else if (str.Contains("action=new"))
                {
                    scanActionCallback("reset");
                }
                else if (workingString.StartsWith("ID."))
                {
                    User u = User.FindUser(UserInfo.username);
                    if (!(workingString == u.Scanner.SerialNumber))
                    {
                        //scanner tidak sesuai by DMR
                        serialIsConnected = false;
                        DialogResult result = MessageBox.Show("Scanner tidak terdaftar / tidak sesuai, Mengakhiri Aplikasi", "Application Closing", MessageBoxButtons.OK);
                        LogoutFunction();
                        formLogout = true;
                        this.Close();
                    }

                }
                else if (!str.Contains("action="))
                {
                    setValue(str);
                }
                
            }

        }
        private void scanActionCallback(string text)
        {
            if (text == "reset")
            {
                dataPublic = new string[0];
                dataPrivate = new string[0];
                if (gridPrivateInfo.InvokeRequired || gridPublicInfo.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(scanActionCallback);
                    this.Invoke(d, new object[] { text });
                }
                else
                {
                    try
                    {
                        ActivityLog log = new ActivityLog();
                        log.LogName = "New BG Validation";
                        log.LogTime = DateTime.Now;
                        log.Remarks = string.Format("{0}|{1}|{2}|{3}", UserInfo.username, "New BG Validation", "Success", "");
                        log.Save();

                        gridPrivateInfo.Rows.Clear();
                        gridPublicInfo.Rows.Clear();

                        lblScanInfo.Text = "Silahkan scan dengan scanner Peruri Code terdaftar";
                        lblScanInfo.BackColor = SystemColors.Window;
                        lblScanInfo.ForeColor = SystemColors.ControlText;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                    }
            }
            }
        }

        private void setValue(string text)
        {
            if (this.gridPrivateInfo.InvokeRequired)
            {

                SetTextCallback d = new SetTextCallback(setValue);
                this.Invoke(d, new object[] { text });
            }
            else
            {

                //Print Data

                Console.WriteLine("Public and Private : " + text);

                //show all string
                //string[] stringSeparators = new string[] { "\r\n" };

                //string[] datas = text.Split(stringSeparators, StringSplitOptions.None);
                //int i = 0;

                //foreach (string data in datas)
                //{
                //    if (data != null && data.Length > 0)
                //    {
                //        i++;
                //        gridPrivateInfo.Rows.Add(new object[] { i, data });
                //    }
                //}
                //
                if (text.Contains("|`|~|")){ 
                    string[] stringSeparators = new string[] { ";" };
                    string[] publicPrivateSeparator = new string[] { "|`|~|" };
                    string publicDataCollection = string.Empty;
                    string privateDataCollection = string.Empty;
                    string[] datas = text.Split(publicPrivateSeparator, StringSplitOptions.None);
                    publicDataCollection = datas[0];
                    privateDataCollection = datas[1];

                    string[] publicData = publicDataCollection.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    string[] privateData = privateDataCollection.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    dataPublic = publicData;
                    dataPrivate = privateData;

                    //counter var
                    int publicCounter = 0;
                    int privateCounter = 0;


                    //Clear Old Info
                    gridPrivateInfo.Rows.Clear();
                    gridPublicInfo.Rows.Clear();

                    //foreach pub
                    foreach (string data in publicData)
                    {
                        if (data != null && data.Length > 0)
                        {
                            publicCounter++;
                            String wording = "";
                            String valueData = "";
                            if (publicCounter == 1)
                            {
                                wording = "Nama Rekanan";
                                valueData = publicData[0];
                            }
                            else if (publicCounter == 2)
                            {
                                wording = "Nomor Dokumen";
                                valueData = publicData[1];
                            }
                            else if (publicCounter == 3)
                            {
                                wording = "Nama Pekerjaan";
                                valueData = publicData[2];
                            }
                            else if (publicCounter == 4)
                            {
                                wording = "Jenis Jaminan";
                                valueData = publicData[3];
                            }
                            else if (publicCounter == 5)
                            {
                                wording = "Tanggal mulai jaminan";
                                string[] pecah = publicData[4].Split(' ');
                                valueData = pecah[0];
                            }
                            else if (publicCounter == 6)
                            {
                                wording = "Tanggal selesai jaminan";
                                string[] pecah = publicData[5].Split(' ');
                                valueData = pecah[0];
                            }
                            //gridPublicInfo.Rows.Add(new object[] { publicCounter, data.Trim() });
                            gridPublicInfo.Rows.Add(new object[] { wording, valueData });
                        }
                    }
                    //foreach private
                    foreach (string data in privateData)
                    {
                        if (data != null && data.Length > 0 && !string.IsNullOrWhiteSpace(data))
                        {
                            privateCounter++;
                            String wording = "";
                            if (privateCounter == 1)
                            {
                                wording = "Penerbit Jaminan";
                            }
                            else if (privateCounter == 2)
                            {
                                wording = "Nomor Jaminan";
                            }
                            else if (privateCounter == 3)
                            {
                                wording = "Nilai Jaminan";
                            }
                            //gridPrivateInfo.Rows.Add(new object[] { privateCounter, data.Trim() });
                            gridPrivateInfo.Rows.Add(new object[] { wording, data.Trim() });
                        }
                    }
                }
                else
                {
                    MessageBox.Show("QR tidak memiliki private data.", "Error", MessageBoxButtons.OK);
                }

            }
        }

        private void setScanStatus(string text)
        {
            if (this.lblScanInfo.InvokeRequired)
            {

                SetTextCallback d = new SetTextCallback(setScanStatus);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                lblScanInfo.Text = text;
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if(e.Argument.ToString() == "action=isValid")
            {
                msg.setResultValid();
            }
            else if(e.Argument.ToString() == "action=validate")
            {
                msg.ShowDialog();
            }
            
        }

        private void menuItemFileConfiguration_Click(object sender, EventArgs e)
        {
            FormConfig config = new FormConfig();
            config.ShowDialog();
        }

        private void menuItemFileExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuItemLogOut_Click(object sender, EventArgs e)
        {
            LogoutFunction();
            formLogout = true;
            this.Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            dataPublic  = new string[0];
            dataPrivate = new string[0];
            try
            {
                ActivityLog log = new ActivityLog();
                log.LogName = "New BG Validation";
                log.LogTime = DateTime.Now;
                log.Remarks = string.Format("{0}|{1}|{2}|{3}", UserInfo.username, "New BG Validation", "Success", "");
                log.Save();

                gridPrivateInfo.Rows.Clear();
                gridPublicInfo.Rows.Clear();

                lblScanInfo.Text = "Silahkan scan dengan scanner Peruri Code terdaftar";
                lblScanInfo.BackColor = SystemColors.Window;
                lblScanInfo.ForeColor = SystemColors.ControlText;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
}

        private void menuItemFileConfiguration_Click_1(object sender, EventArgs e)
        {
            FormConfig cfg = new FormConfig();
            cfg.ShowDialog();
        }

        private void scannerLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormLog log = new FormLog();
            log.ShowDialog();
        }

        private void exportTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataPublic.Length > 0 && dataPrivate.Length > 0)
            {
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);

                PdfPTable pdfTablePublic = new PdfPTable(2);
                pdfTablePublic.DefaultCell.Padding = 10;
                pdfTablePublic.WidthPercentage = 100;
                pdfTablePublic.HorizontalAlignment = Element.ALIGN_LEFT;
                pdfTablePublic.DefaultCell.BorderWidth = 1;
                pdfTablePublic.SpacingAfter = 20;
                pdfTablePublic.SpacingBefore = 20;

                PdfPTable pdfTablePrivate = new PdfPTable(2);
                pdfTablePrivate.DefaultCell.Padding = 10;
                pdfTablePrivate.WidthPercentage = 100;
                pdfTablePrivate.HorizontalAlignment = Element.ALIGN_LEFT;
                pdfTablePrivate.DefaultCell.BorderWidth = 1;
                pdfTablePrivate.SpacingAfter = 20;
                pdfTablePrivate.SpacingBefore = 20;

                iTextSharp.text.Font text = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font textTitleTable = new iTextSharp.text.Font(bf, 15, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font textTitle = new iTextSharp.text.Font(bf, 20 ,iTextSharp.text.Font.BOLD);

                iTextSharp.text.Paragraph title = new iTextSharp.text.Paragraph("TANDA TERIMA", textTitle);
                title.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                title.SpacingAfter = 30;
                title.SpacingBefore = 10;

                iTextSharp.text.Paragraph tanggal = new iTextSharp.text.Paragraph("Tanggal : " + instance.GetTimestamp2(DateTime.Now), text);
                tanggal.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                tanggal.SpacingAfter = 20;
                tanggal.SpacingBefore = 20;

                iTextSharp.text.Paragraph validated = new iTextSharp.text.Paragraph("Validated By : ", text);
                validated.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                validated.SpacingAfter = 50;
                validated.SpacingBefore = 20;

                iTextSharp.text.Paragraph validatedBy = new iTextSharp.text.Paragraph("( " + UserInfo.name + " )", textTitleTable);
                validatedBy.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                validatedBy.SpacingAfter = 20;
                validatedBy.SpacingBefore = 20;

                //ADD TABLE PUBLIC
                var i = 0;
                foreach (String rows in dataPublic)
                {
                    i++;
                    if (i == 1)
                    {
                        pdfTablePublic.AddCell(new Phrase("Nama Rekanan", textTitleTable));
                    }
                    else if (i == 2)
                    {
                        pdfTablePublic.AddCell(new Phrase("Nomor Dokumen", textTitleTable));
                    }
                    else if (i == 3)
                    {
                        pdfTablePublic.AddCell(new Phrase("Nama Pekerjaan", textTitleTable));
                    }
                    else if (i == 4)
                    {
                        pdfTablePublic.AddCell(new Phrase("Jenis Jaminan", textTitleTable));
                    }
                    else if (i == 5)
                    {
                        pdfTablePublic.AddCell(new Phrase("Tanggal mulai jaminan", textTitleTable));
                    }
                    else if (i == 6)
                    {
                        pdfTablePublic.AddCell(new Phrase("Tanggal selesai jaminan", textTitleTable));
                    }

                    if (i == 5 || i == 6)
                    {
                        string[] pecah = rows.Split(' ');
                        pdfTablePublic.AddCell(new Phrase(pecah[0]));
                    }
                    else
                    {
                        pdfTablePublic.AddCell(new Phrase(rows));
                    }
                    
                }

                //ADD TABLE PRIVATE
                var j = 0;
                foreach (String rows in dataPrivate)
                {
                    j++;
                    if (j == 1)
                    {
                        pdfTablePrivate.AddCell(new Phrase("Penerbit Jaminan", textTitleTable));
                    }
                    else if (j == 2)
                    {
                        pdfTablePrivate.AddCell(new Phrase("Nomor Jaminan", textTitleTable));
                    }
                    else if (j == 3)
                    {
                        pdfTablePrivate.AddCell(new Phrase("Nilai Jaminan", textTitleTable));
                    }
                    pdfTablePrivate.AddCell(new Phrase(rows));
                }

                String timeStamp = instance.GetTimestamp(DateTime.Now);

                var saveFileDialog = new SaveFileDialog();
                saveFileDialog.FileName = "tandaterima_"+ timeStamp;
                saveFileDialog.DefaultExt = ".pdf";
                try
                {
                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        using (FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                        {
                            Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 20f, 10f);
                            PdfWriter.GetInstance(pdfDoc, stream);
                            pdfDoc.Open();
                            pdfDoc.Add(title);
                            pdfDoc.Add(tanggal);
                            pdfDoc.Add(new Paragraph("Informasi Umum"));
                            pdfDoc.Add(pdfTablePublic);
                            pdfDoc.Add(new Paragraph("Informasi Rahasia"));
                            pdfDoc.Add(pdfTablePrivate);
                            pdfDoc.Add(validated);
                            pdfDoc.Add(validatedBy);
                            pdfDoc.Close();
                            stream.Close();
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Gagal Export | File sedang dibuka");
                }
            }
            else
            {
                MessageBox.Show("Data tidak ditemukan, harap scan Peruri Code Terlebih Dahulu");
            }
        }
    }
}
