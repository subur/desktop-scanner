﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Collections;
using System.Configuration;

namespace PO.Scanner.App.Forms
{
    public partial class FormConfig : Form
    {
        public FormConfig()
        {
            InitializeComponent();
            txtPortConfig.Text = System.Configuration.ConfigurationManager.AppSettings.Get("COM").ToString();
            txtWebUri.Text = System.Configuration.ConfigurationManager.AppSettings.Get("SyncWebURI").ToString();
            txtSerialNumber.Text = System.Configuration.ConfigurationManager.AppSettings.Get("serialNumber").ToString();
            
            //GET CONNECTION
            string connectionStrings = ConfigurationManager.ConnectionStrings["connection_string_name"].ConnectionString;
            string[] explode = connectionStrings.Split(';');
            string[] vServer = explode[0].Split('=');
            string[] vDatabase = explode[1].Split('=');
            string[] vUserID = explode[2].Split('=');
            string[] vPassword = explode[3].Split('=');
            txtServer.Text = vServer[1];
            txtDatabase.Text = vDatabase[1];
            txtUserID.Text = vUserID[1];
            txtPassword.Text = vPassword[1];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //EDIT SCANNER
            System.Configuration.ConfigurationManager.AppSettings.Set("COM", txtPortConfig.Text);
            System.Configuration.ConfigurationManager.AppSettings.Set("SyncWebURI", txtWebUri.Text);
            System.Configuration.ConfigurationManager.AppSettings.Set("serialNumber", txtSerialNumber.Text);
            

            //EDIT CONNECTION
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings.Remove("COM");
            config.AppSettings.Settings.Add("COM", txtPortConfig.Text);
            
            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
            connectionStringsSection.ConnectionStrings["connection_string_name"].ConnectionString = string.Format("Server=" + txtServer.Text + ";Database=" + txtDatabase.Text + ";Uid=" + txtUserID.Text + ";Pwd=" + txtPassword.Text + ";sslMode=none");
            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSync_Click(object sender, EventArgs e)
        {

            try
            {
                HttpWebRequest req = WebRequest.Create(txtWebUri.Text) as HttpWebRequest;
                req.KeepAlive = false;
                req.Method = "GET";
                HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader loResponseStream = new StreamReader(resp.GetResponseStream(), enc);
                string Response = loResponseStream.ReadToEnd();

                txtSyncLog.Text = Response;
            }catch(Exception exc)
            {
                txtSyncLog.Text = exc.Message;
            }
        }

        private void FormConfig_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
