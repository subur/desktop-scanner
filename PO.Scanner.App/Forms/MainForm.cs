﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PO.Scanner.App.Models;
using NHibernate.Cfg;
using NHibernate;
using NHibernate.Collection;
using System.Security.Cryptography;

namespace PO.Scanner.App.Forms
{
    public partial class MainForm : Form
    {
        private bool login = false;
        public MainForm()
        {
            InitializeComponent();

            ActivityLog log = new ActivityLog();
            log.LogName = "App Open";
            log.LogTime = DateTime.Now;
            log.Remarks = string.Format("{0}|{1}|{2}", UserInfo.username, "App Open", "Success");
            log.Save();

            txtUsername.Text = System.Configuration.ConfigurationManager.AppSettings.Get("defaultUsername");
            txtPassword.Text = System.Configuration.ConfigurationManager.AppSettings.Get("defaultPassword");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginFunction();
        }

        private void txtUsername_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                LoginFunction();
            }
        }
        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                LoginFunction();
            }
        }

        private void LoginFunction()
        {
            try
            {
                User u = new User();
                Common comm = new Common();
                u = u.Login(txtUsername.Text, comm.CalculateMD5Hash(txtPassword.Text));
                if (u != null)
                {
                    UserInfo.username = u.Username;
                    UserInfo.name = u.Name;
                    UserInfo.status = u.Status;
                    UserInfo.ScannerSerialNumber = u.Scanner.SerialNumber;

                    ActivityLog log = new ActivityLog();
                    log.LogName = "Login";
                    log.LogTime = DateTime.Now;
                    log.Remarks = string.Format("{0}|{1}|{2}", UserInfo.username, "Login", "Success");
                    log.Save();

                    login = true;
                    this.Close();
                    FormScan scanForm = new FormScan();
                    scanForm.ShowDialog();
                }
                else
                {
                    lblLoginResult.Text = "Incorrect username / password. Please try again.";
                    ActivityLog log = new ActivityLog();
                    log.LogName = "Login";
                    log.LogTime = DateTime.Now;
                    log.Remarks = string.Format("{0}|{1}|{2}|{3}", txtUsername.Text, "Login", "Failed",lblLoginResult.Text);
                    log.Save();
                }
            }
            catch (Exception exc)
            {
                lblLoginResult.Text = exc.Message;
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!login)
            {
                ActivityLog log = new ActivityLog();
                log.LogName = "App Close";
                log.LogTime = DateTime.Now;
                log.Remarks = string.Format("{0}|{1}|{2}", UserInfo.username, "App Close", "Success");
                log.Save();

                Application.Exit();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormConfig configForm = new FormConfig();
            configForm.ShowDialog();
        }
    }
}
