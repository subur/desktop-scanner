﻿using PO.Scanner.App.Connection;
using MetroFramework;
using MetroFramework.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using PO.Scanner.App.Models;

namespace PO.Scanner.App.Forms
{
    public partial class FormLog : Form
    {

        Stringformat instance = new Stringformat();

        public FormLog()
        {
            InitializeComponent();
        }

        private void FormLog_Load(object sender, EventArgs e)
        {
            try
            {
                ref_dgv_data();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(0);
            }
        }

        private void ref_dgv_data()
        {
            dgv_list.DataSource = select_db(false);
            dgv_list.DataMember = "t_bg_validation";
            show_dgv();
        }

        private object select_db(bool valid)
        {
            if (valid)
            {
                var query = string.Format("SELECT t_bg_validation_id,bg_number,validation_status,ts_validation,validated_by,scan_body FROM t_bg_validation WHERE validation_status='VALID' AND validated_by = '{0}'", UserInfo.username);
                var db = new Connection.Connection().DataGridSelect("t_bg_validation", query);
                return db;
            }
            else
            {
                var query = string.Format("SELECT t_bg_validation_id,bg_number,validation_status,ts_validation,validated_by,scan_body FROM t_bg_validation WHERE validated_by = '{0}'", UserInfo.username);
                var db = new Connection.Connection().DataGridSelect("t_bg_validation", query);
                return db;
            }
        }

        private void show_dgv()
        {
            var kolom0 = dgv_list.Columns[0];
            kolom0.HeaderText = @"VALIDATION ID";

            var kolom1 = dgv_list.Columns[1];
            kolom1.HeaderText = @"NOMOR JAMINAN";

            var kolom2 = dgv_list.Columns[2];
            kolom2.HeaderText = @"VALIDATION STATUS";

            var kolom3 = dgv_list.Columns[3];
            kolom3.HeaderText = @"TS VALIDATION";

            var kolom4 = dgv_list.Columns[4];
            kolom4.HeaderText = @"VALIDATE BY";

            var kolom5 = dgv_list.Columns[5];
            kolom5.HeaderText = @"DATA PERURI CODE";

            this.dgv_list.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_list.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_list.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_list.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_list.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_list.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String timeStamp = instance.GetTimestamp(DateTime.Now);
            exportToPdf(dgv_list, timeStamp);
        }

        private void exportToPdf(DataGridView dataGrid, string filename)
        {
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);
            PdfPTable pdfTable = new PdfPTable(dataGrid.Columns.Count);
            pdfTable.DefaultCell.Padding = 3;
            pdfTable.WidthPercentage = 100;
            pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfTable.DefaultCell.BorderWidth = 1;

            iTextSharp.text.Font text = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);


            //ADD COLUMN HEADER
            foreach (DataGridViewColumn column in dataGrid.Columns)
            {
                PdfPCell pdfCell = new PdfPCell(new Phrase(column.HeaderText, text));
                pdfCell.BackgroundColor = new iTextSharp.text.BaseColor(240, 240, 240);
                pdfCell.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfTable.AddCell(pdfCell);
            }

            //ADD ROW
            foreach (DataGridViewRow rows in dataGrid.Rows)
            {
                var i = 0;
                foreach (DataGridViewCell cell in rows.Cells)
                {
                    i++;
                    if (i == 6)
                    {
                        pdfTable.AddCell(new Phrase(cell.Value.ToString(), text));
                    }
                    else
                    {
                        PdfPCell pdfCell = new PdfPCell(new Phrase(cell.Value.ToString(), text));
                        pdfCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfTable.AddCell(pdfCell);
                    }
                }
            }

            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = filename;
            saveFileDialog.DefaultExt = ".pdf";
            try
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    using (FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                    {
                        Document pdfDoc = new Document(PageSize.A4.Rotate(), 10f, 10f, 10f, 5f);
                        PdfWriter.GetInstance(pdfDoc, stream);
                        pdfDoc.Open();
                        pdfDoc.Add(pdfTable);
                        pdfDoc.Close();
                        stream.Close();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Gagal Export | File sedang dibuka");
            }
        }

        private void date1_ValueChanged(object sender, EventArgs e)
        {
            filter_date();
        }

        private void date2_ValueChanged(object sender, EventArgs e)
        {
            filter_date();
        }

        private void filter_date()
        {
            string awal = date1.Value.ToString();
            string akhir = date2.Value.ToString();
            bool valid = false, cariBG = false;
            if (cb_filterValid.Checked == true)
            {
                valid = true;
            }
            else
            {
                valid = false;
            }
            if (!string.IsNullOrEmpty(tb_cariBG.Text))
            {
                cariBG = true;
            }
            else
            {
                cariBG = false;
            }
            dgv_list.DataSource = select_db_filter(DateTime.Parse(awal), DateTime.Parse(akhir), valid, cariBG);
            show_dgv();
        }

        private object select_db_filter(DateTime awal, DateTime akhir, bool valid, bool cariBG)
        {
            string dateawal = awal.ToString("yyyy-MM-dd");
            string dateakhir = akhir.ToString("yyyy-MM-dd");
            if (valid && cariBG)
            {
                var query = string.Format("SELECT t_bg_validation_id,bg_number,validation_status,ts_validation,validated_by,scan_body FROM t_bg_validation WHERE validation_status='valid' AND validated_by = '{0}' AND bg_number like '%{1}%' AND DATE(ts_validation) BETWEEN '{2}' AND '{3}'", UserInfo.username, tb_cariBG.Text, dateawal, dateakhir);
                var db = new Connection.Connection().DataGridSelect("t_bg_validation", query);
                return db;
            }
            else if (!valid && cariBG)
            {
                var query = string.Format("SELECT t_bg_validation_id,bg_number,validation_status,ts_validation,validated_by,scan_body FROM t_bg_validation WHERE validated_by = '{0}' AND bg_number like '%{1}%' AND DATE(ts_validation) BETWEEN '{2}' AND '{3}'", UserInfo.username,tb_cariBG.Text, dateawal, dateakhir);
                var db = new Connection.Connection().DataGridSelect("t_bg_validation", query);
                return db;
            }
            else if (valid && !cariBG)
            {
                var query = string.Format("SELECT t_bg_validation_id,bg_number,validation_status,ts_validation,validated_by,scan_body FROM t_bg_validation WHERE validation_status='VALID' AND validated_by = '{0}' AND DATE(ts_validation) BETWEEN '{1}' AND '{2}'",UserInfo.username, dateawal, dateakhir);
                var db = new Connection.Connection().DataGridSelect("t_bg_validation", query);
                return db;
            }
            else
            {
                var query = string.Format("SELECT t_bg_validation_id,bg_number,validation_status,ts_validation,validated_by,scan_body FROM t_bg_validation WHERE validated_by = '{0}' AND DATE(ts_validation) BETWEEN '{1}' AND '{2}'",UserInfo.username, dateawal, dateakhir);
                var db = new Connection.Connection().DataGridSelect("t_bg_validation", query);
                return db;
            }
        }

        private void tb_cariBG_TextChanged(object sender, EventArgs e)
        {
            string awal = date1.Value.ToString();
            string akhir = date2.Value.ToString();
            bool valid = false, cariBG = false;
            if (cb_filterValid.Checked == true)
            {
                valid = true;
            }
            else
            {
                valid = false;
            }
            if (!string.IsNullOrEmpty(tb_cariBG.Text))
            {
                cariBG = true;
            }
            else
            {
                cariBG = false;
            }
            dgv_list.DataSource = select_db_filter(DateTime.Parse(awal), DateTime.Parse(akhir), valid, cariBG);
            show_dgv();
        }

        private void cb_filterValid_CheckedChanged(object sender, EventArgs e)
        {
            string awal = date1.Value.ToString();
            string akhir = date2.Value.ToString();
            bool valid = false, cariBG = false;
            if (cb_filterValid.Checked == true)
            {
                valid = true;
            }
            else
            {
                valid = false;
            }
            if (!string.IsNullOrEmpty(tb_cariBG.Text))
            {
                cariBG = true;
            }
            else
            {
                cariBG = false;
            }
            dgv_list.DataSource = select_db_filter(DateTime.Parse(awal), DateTime.Parse(akhir), valid, cariBG);
            show_dgv();
        }

        private void btExportExcel_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("AAAAA");
            Microsoft.Office.Interop.Excel.Application apps = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook workbook = apps.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = null;

            //apps.Visible = true;

            worksheet = workbook.Sheets["Sheet1"];
            worksheet = workbook.ActiveSheet;
            worksheet.Name = "HasilValidasi";

            for (int i = 1; i < dgv_list.Columns.Count + 1; i++)
            {
                worksheet.Cells[1, i] = dgv_list.Columns[i - 1].HeaderText;
            }
            for (int i = 0; i < dgv_list.Rows.Count; i++)
            {
                for (int j = 0; j < dgv_list.Columns.Count; j++)
                {
                    worksheet.Cells[i + 2, j + 1] = dgv_list.Rows[i].Cells[j].Value.ToString();
                }
            }

            var saveFile = new SaveFileDialog();
            saveFile.FileName = "Validasi_" + instance.GetTimestamp(DateTime.Now);
            saveFile.DefaultExt = ".xlsx";
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                workbook.SaveAs(saveFile.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing,Type.Missing);
            }
            apps.Quit();
        }
    }
}
