﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using com.google.zxing;
using com.google.zxing.qrcode;
using System.IO.Ports;


namespace PO.Scanner.App.Forms
{
    public partial class FormMessage : Form
    {
        public FormMessage()
        {
            InitializeComponent();
        }

        public FormMessage(string Message)
        {
            InitializeComponent();
            QRCodeWriter writer = new QRCodeWriter();
            //btnValid.Image = writer.encode("action=isValid", BarcodeFormat.QR_CODE, 150, 150).ToBitmap();
            //btnInvalid.Image = writer.encode("action=isInvalid", BarcodeFormat.QR_CODE, 150, 150).ToBitmap();
            
            btnValid.BackColor = Color.White;
            btnInvalid.BackColor = Color.White;
            lblMessage.Text = Message;

        }

        private void btnValid_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnInvalid_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        public void setText(string Message)
        {
            //QRCodeWriter writer = new QRCodeWriter();
            //btnValid.Image = writer.encode("action=isValid", BarcodeFormat.QR_CODE, 100, 100).ToBitmap();
            //btnInvalid.Image = writer.encode("action=isInvalid", BarcodeFormat.QR_CODE, 100, 100).ToBitmap();
            //btnValid.BackColor = Color.White;
            //btnInvalid.BackColor = Color.White;
            lblMessage.Text = Message;
        }

        public void setResultValid()
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        public void setResultInvalid()
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}
